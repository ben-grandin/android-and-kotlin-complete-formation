package com.example.helloworld.examples

abstract class Vehicle(val wheelsCount: Int) {

	companion object Factory {
		fun createCar() = Car(4)
		fun createMotorcycle() = Motorcycle(4)
	}
}

class Car(wheelsCount: Int) : Vehicle(wheelsCount)

class Motorcycle(wheelsCount: Int) : Vehicle(wheelsCount)


fun companionObject() {
	val car = Vehicle.createCar()
	val motorcycle =
		Vehicle.createMotorcycle()
}