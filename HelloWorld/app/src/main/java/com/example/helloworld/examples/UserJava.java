package com.example.helloworld.examples;

public class UserJava {
    private String name;
    private Address address;
    private int age;

    public UserJava() {
        address = new Address("Paris", "France");
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAge(String age) {
        this.age = age.codePointAt(0);
    }
}
