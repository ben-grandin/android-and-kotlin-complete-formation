package com.example.helloworld.examples

data class Address(val city: String, val country: String)