package com.example.helloworld.examples

import android.util.Log

fun testExamples() {
	//	generics()
	//	lateinit()
	//	companionObject()

	// Use Java in Kotlin
	val bob = UserJava()
	bob.age = 5
	bob.setAge("10")
	bob.name = "Bob"

	// Use Kotlin in Java
	Log.i("MainActivity", "Adresse de bob est ${bob.address}")
}