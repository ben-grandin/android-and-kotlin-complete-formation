package com.example.helloworld

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)

		setSupportActionBar(toolbar)

		val user = User("Bob", 10)
		show_user_details_button.setOnClickListener {
			val intent = Intent(this, UserDetailActivity::class.java)
			intent.putExtra("user", user)
			startActivity(intent)
		}

		test_linear_layout.setOnClickListener {
			val intent = Intent(this, TestLinearActivity::class.java)
			startActivity(intent)
		}

		test_relative_layout.setOnClickListener {
			val intent = Intent(this, TestRelativeLayout::class.java)
			startActivity(intent)
		}

		test_ui_bases_components.setOnClickListener {
			val intent = Intent(this, TestUiBasesComponents::class.java)
			startActivity(intent)
		}

		show_dialog.setOnClickListener {
			val fragment = ConfirmDeleteDialogFragment()
			// ToDo: Don't works, deprecated
			//fragment.show(fragmentManager, "ConfirmDelete")
		}

	}

	override fun onCreateOptionsMenu(menu: Menu?): Boolean {
		menuInflater.inflate(R.menu.main_menu, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		return when (item.itemId) {
			R.id.action_add -> {
				Toast.makeText(this, "Ajouter", Toast.LENGTH_SHORT).show()
				true
			}
			R.id.action_delete -> {
				Toast.makeText(this, "Supprimer", Toast.LENGTH_SHORT).show()
				true
			}
			R.id.action_help -> {
				val intent = Intent(this, FavoriteActivity::class.java)
				startActivity(intent)
				true
			}
			else -> super.onOptionsItemSelected(item)
		}
	}
}



